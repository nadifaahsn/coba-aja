from django.shortcuts import render

def index(request):
    riwayat = ['TKIT Ummul Quro', 'SDIT Ummul Quro', 'SMPIT Ummul Quro', 'SMAN 5 Bogor', 'Universitas Indonesia']
    context =  {
        'nama' : 'Hasna Nadifah',
        'NPM' : '1906293096',
        'riwayat' : riwayat,
    }
    return render(request, 'index.html', context)